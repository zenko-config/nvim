return {
	"VonHeikemen/lsp-zero.nvim",
	config = function()
		local lsp_zero = require("lsp-zero").preset({
			name = "manual-setup",
		})

		lsp_zero.on_attach(function(_, bufnr)
			lsp_zero.default_keymaps({
				buffer = bufnr,
				preseve_mapping = false,
				exclude = { "<F3>", "<F4>" },
			})

			vim.keymap.set("n", "<leader>a", vim.lsp.buf.code_action, {
				buffer = bufnr,
				desc = "Code action",
			})
		end)

		lsp_zero.setup()
	end,
}
