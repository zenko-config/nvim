return {
  "epwalsh/obsidian.nvim",
  lazy = true,
  ft = "markdown",
  opts = require 'options.obsidian',
}
