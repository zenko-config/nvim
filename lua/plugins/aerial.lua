return {
	'stevearc/aerial.nvim',
	keys = require 'keymaps.aerial',
	opts = require 'options.aerial',
}
