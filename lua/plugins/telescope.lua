return {
	"nvim-telescope/telescope.nvim",
	lazy = false,
	keys = require "keymaps.telescope",
	opts = require "options.telescope",
}
