return {
	"mistweaverco/kulala.nvim",
	opts = require 'options.kulala',
	config = function(_, opts)
		require('kulala').setup(opts)
	end
}
