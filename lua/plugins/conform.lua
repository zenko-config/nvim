return {
	"stevearc/conform.nvim",
	event = { "BufWritePre" },
	keys = require("keymaps.conform"),
	opts = require("options.conform"),
}
