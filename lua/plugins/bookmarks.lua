return {
	'tomasky/bookmarks.nvim',
	lazy = false,
	keys = require 'keymaps.bookmarks',
	opts = require 'options.bookmarks',
	config = function(_, opts)
		require('bookmarks').setup(opts)
		require('telescope').load_extension('bookmarks')
	end
}
