return {
	'ahmedkhalf/project.nvim',
	lazy = false,
	keys = require 'keymaps.project',
	opts = require 'options.project',
	config = function (_, opts)
		require('project_nvim').setup(opts)

		require('telescope').load_extension('projects')
	end
}
