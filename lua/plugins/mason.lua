return {
	"williamboman/mason.nvim",
	lazy = false,
	keys = require 'keymaps.mason',
	opts = require 'options.mason',
}
