return {
	"rcarriga/nvim-notify",
	opts = require "options.nvim-notify",
	init = function()
		vim.opt.termguicolors = true
		
		vim.notify = require "notify"
	end
}
