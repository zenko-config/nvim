return {
	"catppuccin/nvim",
	lazy = false,
	priority = 1000,
	opts = require("options.catppuccin"),
	config = function(_, opts)
		require("catppuccin").setup(opts)

		vim.cmd.colorscheme "catppuccin"
	end,
}
