return {
	'hrsh7th/nvim-cmp',
	event = 'InsertEnter',
	opts = require 'options.nvim-cmp',
	config = function(_, opts)
		require('lsp-zero').extend_cmp()
		local cmp = require 'cmp'

		cmp.setup({
			source = opts.sources,
			mapping = cmp.mapping.preset.insert({
				['<C-space>'] = cmp.mapping.complete(),
				['<C-u>'] = cmp.mapping.scroll_docs(-4),
				['<C-d>'] = cmp.mapping.scroll_docs(4),
				['<CR>'] = cmp.mapping.confirm({
					behavior = cmp.ConfirmBehavior.Replace,
					select = false
				}),
			}),
		})
	end
}
