return {
	'stevearc/oil.nvim',
	keys = require 'keymaps.oil',
	opts = require 'options.oil',
}
