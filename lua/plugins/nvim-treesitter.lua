return {
	"nvim-treesitter/nvim-treesitter",
	opts = require 'options.nvim-treesitter',
	config = function(_, opts)
		require('nvim-treesitter.configs').setup(opts)
	end
}
