return {
	'neovim/nvim-lspconfig',
	cmd = {'LspInfo', 'LspInstall', 'LspStart'},
	event = { 'BufReadPre', 'BufNewFile' },
	opts = require 'options.nvim-lspconfig',
	config = function(_, opts)
		require('lsp-zero').extend_lspconfig()

		for handler, handler_options in pairs(opts.handlers) do
			if handler_options.enabled then
				require('lspconfig')[handler].setup(handler_options)
			end
		end
	end,
}
