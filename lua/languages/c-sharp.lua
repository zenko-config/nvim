return {
	treesitter = {
		enabled = true,
	},
	lsp = {
		enabled = true,
		name = "csharp_ls",
	},
	formatter = {
		enabled = true,
		name = "clang-format",
	},
}
