return {
	treesitter = {
		enabled = true,
	},
	lsp = {
		enabled = true,
		name = "lua_ls",
	},
	formatter = {
		enabled = true,
		name = 'stylua',
	},
}
