return {
	treesitter = {
		enabled = true,
	},
	lsp = {
		enabled = true,
		name = "rust_analyzer",
	},
	formatter = {
		enabled = true,
		name = 'rustfmt',
	},
}
