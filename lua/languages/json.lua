return {
	treesitter = {
		enabled = true,
	},
	lsp = {
		enabled = true,
		name = "jsonls"
	},
	formatter = {
		enabled = true,
		name = 'fixjson'
	}
}
