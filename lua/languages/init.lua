return {
	json = require("languages.json"),
	http = require("languages.http"),
	lua = require("languages.lua"),
	markdown = require("languages.markdown"),
	rust = require("languages.rust"),
	c_sharp = require("languages.c-sharp"),
}
