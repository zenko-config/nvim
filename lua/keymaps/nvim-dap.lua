return {
	{
		'<leader>tb',
		function ()
			require('dap').toggle_breakpoint()
		end,
		desc = 'Toggle a breakpoint for dap',
	},
	{
		'<leader>dr',
		function ()
			require('dap').continue()
		end,
		desc = 'Run/Continue Debugger',
	},
	{
		'<leader>di',
		function ()
			require('dap').step_into()
		end,
		desc = 'Step into debugger',
	},
	{
		'<leader>do',
		function ()
			require('dap').step_over()
		end,
		desc = 'Step over debugger',
	}
}
