return {
    { 
		"<leader>kp", 
		function() require('kulala').jump_prev() end, 
		desc = "Jump to previous request" 
	},
    { 
		"<leader>kn", 
		function() require('kulala').jump_next() end, 
		desc = "Jump to next request" 
	},
    { 
		"<leader>kr", 
		function() require('kulala').run() end, 
		desc = "Run request" 
	}
}
