return {
	{
		'<leader>op',
		function() require('telescope').extensions.projects.projects() end,
		desc = 'Open projects',
	}
}
