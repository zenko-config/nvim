return {
	{
		"<leader>ff",
		"<cmd>Telescope find_files<cr>",
		desc = "Fuzzy find files in cwd",
	},
	{
		"<leader>fg",
		"<cmd>Telescope live_grep<cr>",
		desc = "Live grep file in cwd",
	}
}
