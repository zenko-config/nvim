return {
	{
		"<F3>",
		function()
			require("conform").format({ async = true })
		end,
		desc = "Format buffer",
	},
}
