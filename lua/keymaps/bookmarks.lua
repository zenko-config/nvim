return {
	{
		'<leader>om',
		function()
			require('telescope').extensions.bookmarks.list()
		end,
		desc = 'Open bookmarks in telescope',
	},
	{
		'<leader>tm',
		function()
			require('bookmarks').bookmark_toggle()
		end,
		desc = 'Toggle a bookmarks',
	},
	{
		'mi',
		function()
			require('bookmarks').bookmark_ann()
		end,
		desc = 'Add or edit a bookmark on this line',
	},
	{
		'mc',
		function()
			require('bookmarks').bookmark_clear()
		end,
		desc = 'Clean all bookmarks on this buffer',
	},
	{
		'mx',
		function()
			require('bookmarks').bookmark_clear_all()
		end,
		desc = 'Clear all bookmarks',
	},
}
