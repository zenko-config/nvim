local languages = require 'languages'

local handlers = {}

for language, options in pairs(languages) do
	if options.lsp then 
		handlers[options.lsp.name] = {
			enabled = options.lsp.enabled,
			options = {},
		}
		if options.lsp.opts then
			handlers[options.lsp.name].options = options.lsp.opts
		end
	end
end

return {
	handlers = handlers
}
