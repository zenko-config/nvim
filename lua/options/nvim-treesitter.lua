local languages = require 'languages'

local ensure_installed = {}

for language, options in pairs(languages) do
	if options.treesitter and options.treesitter.enabled then
		table.insert(ensure_installed, language)
	end
end

return {
	ensure_installed = ensure_installed,
	sync_install = false,
	auto_install = false,
}
