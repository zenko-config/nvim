return {
	flavour = "mocha",
	transparent_background = false,
	integration = {
		notify = true,
		which_key = true,
		gitsigns = true,
		telescope = true,
		nvimtree = true,
	}
}
