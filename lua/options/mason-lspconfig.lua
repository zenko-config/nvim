local languages = require 'languages'

local ensure_installed = {}

for language, options in pairs(languages) do
	if options.lsp and options.lsp.enabled then
		table.insert(ensure_installed, options.lsp.name)
	end
end

return {
	ensure_installed = ensure_installed,
	automatic_installation = false,
}
