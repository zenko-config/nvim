local languages = require("languages")

local formatters = {
	-- Default formatters that trim whitespace
	["_"] = { "trim_whitespace" },
}

for language, options in pairs(languages) do
	if options.formatter and options.formatter.enabled then
		formatters[language] = { options.formatter.name }
	end
end

return {
	formatters_by_ft = formatters,
	format_on_save = {
		timeout_ms = 500,
		lsp_fallback = true,
	},
}
